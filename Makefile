.PHONY: test rpm deb

test:
	./src/avro-check.lua --test true

test-release: test
	@echo "############ Relese tests ################"
	@echo "#### Correct object"
	@./src/avro-check.lua -c ./src/default.conf '{"body": "sedan", "weight": 1500, "length": 2.5, "engine_volume": 1.8, "color": "white", "brand": "Opel"}' && echo "Success"
	@echo "#### Incorrect object"
	@./src/avro-check.lua -c ./src/default.conf '{"weight": 1500, "length": 2.5, "engine_volume": 1.8, "color": "white", "brand": "Opel"}' || ([ $$? -eq 1 ] && echo "Success" || exit 1 )
	@echo "#### Incorrect JSON"
	@./src/avro-check.lua -c ./src/default.conf '{"brand": "Opel",}' || ([ $$? -eq 1 ] && echo "Success" || exit 1 )
	@echo "#### Missing configuration file"
	@./src/avro-check.lua -c ./src/xxx.conf '{"brand": "Opel"}' || ([ $$? -eq 1 ] && echo "Success" || exit 1 )
	@echo "################# end ####################"

rpm:
	fpm -s dir -t rpm -n avro-check --rpm-os linux -a x86-64  \
		--license "MIT Licence" \
		--rpm-summary "Easy utility to verify object over schema in avro-schema format" \
		--url "https://gitlab.com/opomuc/tpos-project" \
		--vendor "Opomuc" \
		--depends "tarantool >= 1.10.0" \
		--depends "lua-argparse >= 0.1.0" \
		--depends "tarantool-avro-schema >= 3.0.0" \
		--config-files /etc/avro-check/check.conf \
		-m "Roman Proskin <proskin@phystech.edu>" \
		--version `git describe --abbrev=0 || echo "0.1.0"` \
		./src/avro-check.lua=/usr/bin/avro-check \
		./src/default.conf=/etc/avro-check/check.conf

deb:
	fpm -s dir -t deb -n avro-check --rpm-os linux -a amd64  \
		--license "MIT Licence" \
		--rpm-summary "Easy utility to verify object over schema in avro-schema format" \
		--url "https://gitlab.com/opomuc/tpos-project" \
		--vendor "Opomuc" \
		--depends "tarantool >= 1.10.0" \
		--depends "lua-argparse >= 0.1.0" \
		--depends "tarantool-avro-schema >= 3.0.0" \
		--config-files /etc/avro-check/check.conf \
		-m "Roman Proskin <proskin@phystech.edu>" \
		--version `git describe --abbrev=0 || echo "0.1.0"` \
		./src/avro-check.lua=/usr/bin/avro-check \
		./src/default.conf=/etc/avro-check/check.conf

deps:
	cd argparse && \
	fpm -s dir -t deb -n lua-argparse --rpm-os linux -a noarch  \
		--license "MIT Licence" \
		--prefix /usr/share/tarantool \
		--rpm-summary "Argparse library for Lua" \
		--url "https://github.com/mpeterv/argparse" \
		--version `git describe --abbrev=0 || echo "0.1.0"` \
		./src/argparse.lua=argparse.lua \
	&& \
	fpm -s dir -t rpm -n lua-argparse --rpm-os linux -a noarch  \
		--license "MIT Licence" \
		--prefix /usr/share/tarantool \
		--rpm-summary "Argparse library for Lua" \
		--url "https://github.com/mpeterv/argparse" \
		--version `git describe --abbrev=0 || echo "0.1.0"` \
		./src/argparse.lua=argparse.lua
