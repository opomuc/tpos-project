#!/usr/bin/env tarantool

local test_schema = {
    type = 'record',
    name = 'Car',
    fields = {
        {
            name = 'body', type = {
                type = 'enum',
                name = 'body_type',
                symbols = { 'fourwheeler', 'sedan', 'hatchback', 'coupe' }
            }
        },
        { name = 'engine_volume', type = 'double' },
        { name = 'class', type = 'string*' },
        { name = 'brand', type = 'string' },
        { name = 'weight', type = 'int' },
        { name = 'length', type = 'float' },
        { name = 'color', type = 'string' },
        { name = 'car_plate', type = 'string*' },
    }
}

return {
    schema = test_schema
}
