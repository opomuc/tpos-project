#!/usr/bin/env tarantool

local argparse = require('argparse')
local avro = require('avro_schema')
local json = require('json')
local fio = require('fio')

local parser = argparse("This script validates an object over avro-schema", "An example")
parser:argument("input", "Input object to validate in json format", '{}')
parser:option("-c --config", "Configuration file with avro-schema", "/etc/avro-checker/check.conf")
parser:option("--test", "Run tests", false)

local args = parser:parse()

local function validate(schema, obj)
    return avro.validate(schema, obj)
end

if args.test then
    local test_schema = require('./src/test').schema
    local ok, schema = avro.create(test_schema)
    if not ok then
        print("Incorrect test schema:", schema)
        os.exit(1)
    end
    local test = require('tap').test("Avro-checker tests")
    test:plan(5)

    local test_obj_1 = {
        brand = 'Mitsubishi',
        body = 'fourwheeler',
        weight = 2000,
        length = 3.5,
        engine_volume = 2.8,
        color = 'black',
        class = 'C',
        car_plate = 'A123МХ777РУС'
    }

    test:is(validate(schema, test_obj_1), true, "Full object is OK")
    test_obj_1.class = nil
    test:is(validate(schema, test_obj_1), true, "Missing class is OK")
    test_obj_1.car_plate = nil
    test:is(validate(schema, test_obj_1), true, "Missing car plate is OK")
    test_obj_1.brand = nil
    test:is(validate(schema, test_obj_1), false, "Missing brand is NOT OK")
    test_obj_1.brand = 'Mitsubishi'
    test_obj_1.body = nil
    test:is(validate(schema, test_obj_1), false, "Missing body is NOT OK")

    os.exit(test:check() and 0 or 1)
end

local ok, config_file = pcall(fio.open, args.config)
if not config_file then
    print("Missing configuration file at", args.config)
    os.exit(1)
end

local ok, config = pcall(json.decode, config_file:read())
if not ok then
    print("Config is incorrect:", config)
    os.exit(1)
end

local ok, schema = avro.create(config)
if not ok then
    print("Incorrect avro schema:", schema)
    os.exit(1)
end

local ok, validation_obj = pcall(json.decode, args.input)
if not ok then
    print("Incorrect input:", args.input)
    os.exit(1)
end

local ok, err = validate(schema, validation_obj)
if not ok then
    print("Incorrect object:", err)
else
    print("Provided object is correct.")
end

os.exit(ok and 0 or 1)
